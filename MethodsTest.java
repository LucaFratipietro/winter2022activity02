public class MethodsTest{
	public static void main (String [] args){
		int x = 10;
		double y = 11.1;
		int z = methodNoInputReturnInt();
		System.out.println("The value for z is " + z);
		
		int squareOne = 6;
		int squareTwo = 3;
		double squareRoot = sumSquareRoot(squareOne, squareTwo);
		System.out.println(squareRoot);
		
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		int k = 50;
		int kAddOne = SecondClass.addOne(k);
		System.out.println(kAddOne);
		
		SecondClass sc = new SecondClass();
		int kAddTwo = sc.addTwo(k);
		System.out.println(kAddTwo);
		
		
	}
	
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int x){
		System.out.println("Inside the method one input no return");
		System.out.println(x);
	}
	
	public static void methodTwoInputNoReturn(int x, double y){
		System.out.println("Here's the int (" + x + ") value");
		System.out.println("Here's the double (" + y + ") value");
	}
	
	public static int methodNoInputReturnInt(){
		return 6;
	}
	
	public static double sumSquareRoot(int x, int y){
		int thirdVariable = x + y;
		double squareRoot = Math.sqrt(thirdVariable);
		return squareRoot;
	}
	
	
}
	
		
		