import java.util.Scanner;

public class PartThree {
	
	public static void main (String [] args){
		Scanner reader = new Scanner (System.in);
		
		int lengthSquare = reader.nextInt();
		int lengthRectangle = reader.nextInt();
		int widthRectangle = reader.nextInt();
		
		AreaComputations ar = new AreaComputations();
		
		int squareArea  = AreaComputations.areaSquare(lengthSquare);
		System.out.println(squareArea);
		
		int rectangleArea = ar.areaRectangle(lengthRectangle, widthRectangle);
		System.out.println(rectangleArea);
	}
	
	
	
}